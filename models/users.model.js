const sql = require("./db.js");

// constructor
const Users = function(user) {
    this.id = user.id;
    this.nombre = user.nombre;
    this.confirmo = user.confirmo;
    this.fecha_confirmo = user.fecha_confirmo;
    this.contrasenia = user.contrasenia;
    this.acompaniante = user.acompaniante;
    this.ire_solo = user.ire_solo;
    this.asistira = user.asistira;
    this.no_asistira = user.no_asistira;
    this.llego_salon = user.llego_salon;
    this.mesa = user.mesa;
    this.asiento = user.asiento;
    this.mesa_acompaniante = user.mesa_acompaniante;
    this.asiento_acompaniante = user.asiento_acompaniante;
    this.nota_voz = user.nota_voz;
    this.qr = user.qr;
    this.nivel = user.nivel;
};
// VARIABLES PARA QUERYS

// ----------------------------------------------------------------------
Users.login = (loger, result) => {
    // console.log('loger', loger);
    sql.query(`SELECT 
                    id
                    ,nombre
                    ,confirmo 
                    ,fecha_confirmo 
                    ,acompaniante
                    ,ire_solo
                    ,asistira
                    ,no_asistira
                    ,llego_salon 
                    ,mesa
                    ,asiento
                    ,mesa_acompaniante
                    ,asiento_acompaniante
                    ,nota_voz AS notaVoz
                    ,nivel
                    ,qr
                FROM invitados 
                WHERE contrasenia = ? AND no_asistira = 0`, [loger.password], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return
        }
        console.log('respuesta', res);
        result(null, res)
    });
};

// ----------------------------------------------------------------------
Users.confirmacion_invitado = (user, result) => {
    sql.query(`UPDATE invitados 
                SET ire_solo = ?
                    , asistira = ?
                    , no_asistira = ?
                    , confirmo = ?
                    , qr = ?
                    , fecha_confirmo = NOW()
                WHERE id = ?`, [
                    user.ire_solo,
                    user.asistira,
                    user.no_asistira,
                    user.confirmo,
                    user.qr,
                    user.id
                ], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        if (res.affectedRows == 0) {
            result({ kind: "not_found" }, null);
            return;
        }
        console.log("Se actualizó el usuario: ", { ...user });
        result(null, { ...user });
    });
};

// ----------------------------------------------------------------------
Users.obtener_lista_invitados = (result) => {
    sql.query(`SELECT 
                    id,
                    nombre,
                    confirmo,
                    fecha_confirmo,
                    contrasenia,
                    mesa,
                    asiento,
                    mesa_acompaniante,
                    asiento_acompaniante,
                    CASE 
                        WHEN acompaniante = 1 AND 
                            ire_solo 	  = 0 AND
                            no_asistira  = 0 AND
                            asistira	  = 1
                        THEN 'LLEVARE ACOMPAÑANTE' 
                        WHEN ire_solo 	  = 0 AND
                            no_asistira  = 0 AND
                            asistira	  = 1
                        THEN 'CLARO QUE ASISTIRE'
                        WHEN ire_solo 	  = 1 AND
                            no_asistira  = 0 AND
                            asistira	  = 1 
                        THEN 'GRACIAS PERO IRE SOLO'
                        WHEN ire_solo 	  = 0 AND
                            no_asistira  = 1 AND
                            asistira	  = 0 
                        THEN 'NO ASISTIRE'
                    ELSE 'SIN CONFIRMAR'
                END AS estatus
                FROM invitados
                WHERE nivel = 1
                ORDER BY confirmo DESC`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return
        }
        console.log("res:- ", res);
        result(null, res)
    });
};

Users.buscar_usuario_QR = (d) => {
    return new Promise((resolve, reject) => {
        sql.query(`SELECT 
                    u.id
                   ,u.nombre
                   ,u.qr
                   ,u.mesa 
                   ,u.asiento
                   ,u.mesa_acompaniante
                   ,u.asiento_acompaniante
              FROM invitados AS u 
              WHERE 
                u.qr = ?`, [d.qr], (err, res) => {
            if (err) {
                reject(err)
                return;
            }
            resolve(res)
        });
    })
};

Users.marcar_usuario_recepcion = (idUsuario) => {
    return new Promise((resolve, reject) => {
        sql.query(`UPDATE invitados 
                    SET llego_salon = 1 
                    WHERE id = ?`, [idUsuario], (err, res) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(res)
        });
    })
};

module.exports = Users;