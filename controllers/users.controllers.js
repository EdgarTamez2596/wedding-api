const Users = require("../models/users.model.js");

exports.iniciar_sesion = (req, res) => {
    Users.login(req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Lo sentimos esté código no tiene acceso.`
                });
            } else {
                res.status(500).send({
                    message: "Error al buscar invitado, intentelo nuevamente"
                });
            }
        } else{
            console.log('data', data);
            res.send(data);
        }
    });
};

exports.confirmacion_invitado = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "El Contenido no puede estar vacio!"
        });
    }

    Users.confirmacion_invitado(req.body, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `No se encontro el invidado con el id ${req.params.id }.`
                });
            } else {
                res.status(500).send({
                    message: "Error al confirmar invitado con el id" + req.params.id
                });
            }
        } else {
            res.send(data);
        }
    });
};

exports.obtener_lista_invitados = (req, res) => { // Retrieve all userss from the database.
    Users.obtener_lista_invitados((err, data) => {
        if (err){
            console.log('err in model', err);
            res.status(500).send({
                message: err.message || "Se produjo algún error al recuperar los usuarios"
            }); 
        }else{
            console.log('data', data);
            res.send(data);
        }
    });
};

exports.captura_recepcion = async(req, res) => {
    try {
        if (!req.body) {
            res.status(500).send({ message: "No se recibio información." });
        } else {
            const usuario = await Users.buscar_usuario_QR(req.body);
            console.log('usuario', usuario);
            if(!usuario.length){
                res.status(500).send({ msg: "Esté codigó no se encuentra en la lista de invitados"});
                return;
            }

            await Users.marcar_usuario_recepcion(usuario[0].id);
            res.status(200).send(usuario);
        }

    } catch (error) {
        res.status(500).send({
            message: error.message || "Se produjo algún error al consultar el tipo de usuario"
        })
    }
};

