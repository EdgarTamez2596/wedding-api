module.exports = app => {
    const users = require('../controllers/users.controllers') // --> ADDED THIS

    // Iniciar Sesion 
    app.post("/iniciar_sesion", users.iniciar_sesion);
    app.post("/confirmacion_invitado", users.confirmacion_invitado);
    app.post("/obtener_lista_invitados", users.obtener_lista_invitados);
    app.post("/capturar_recepcion", users.captura_recepcion);


    
    // app.put("/updateuser/:idusuariosweb", users.updatexId);
    // Create a new Users
    // app.post("/user.add", users.create);
    // Retrieve all users
    // app.get("/users", users.findAll);
    // Retrieve a single Customer with customerId
    // app.get("/users/:userId", users.findOne);
    // Traer un usuario por email
    // app.post("/user.email", users.getxEmail);
    // Update a Customer with customerId
    // app.put("/users/:userId", users.update);
    // Activar usuario
    // app.put("/usuario.activar/:idusuario", users.activarUsuario);

    // app.post("/olvida.contra", users.OlvideContra);
    // app.post("/password.extra", users.passwordExtra);

    // FUERA DE LOGIN
    // app.post("/obtener.tipo.usuario", users.obtenerTipoUsuario);
    // app.get("/obtener.niveles", users.obtenerNiveles)
    // app.post("/cambio.contrasenia", users.cambioContrasenia);
    // app.get("/obtener.usuarios.admin", users.usuariosAdmin)

};