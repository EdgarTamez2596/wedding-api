// IMPORTAR DEPENDENCIAS
const express = require("express");
const bodyParser = require("body-parser");
var cors = require('cors');
const fileUpload = require('express-fileupload')

// Para usar con certificado ssl
var http = require('http');
var https = require('https');
var fs = require('fs');


// IMPORTAR EXPRESS
const app = express();

// Rutas estaticas
// app.use('/fotos-epic-grass', express.static('./../../fotos-epic-grass'));
// app.use('/pdfs', express.static('./../../pdf'));

// IMPORTAR PERMISOS
app.use(cors());
// parse requests of content-type: application/json
app.use(bodyParser.json({ limit: '50mb' }));
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(fileUpload()); //subir archivos


// ----IMPORTAR RUTAS---------------------------------------->
require('./routes/users.routes')(app);

// ----FIN-DE-LAS-RUTAS-------------------------------------->

// DEFINIT PUERTO EN EL QUE SE ESCUCHARA
app.listen(3025, () => {
    console.log("|****************** S-O-F-S-O-L-U-T-I-O-N *********************| ");
    console.log("|******************** C-R-E-A-T-I-B-E *************************| ");
    console.log("|**************************************************************| ");
    console.log("|************ Servidor Corriendo en el Puerto 3025 ************| ");
});